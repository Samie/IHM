﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using DevComponents.WPF.Metro;

namespace MetroOSSample
{
    public class Invoice
    {
        public Invoice(DateTime date, string clientName, double amount, string fileName, int number, string description, string clientID)
        {
            Date = date;
            ClientName = clientName;
            Amount = amount;
            FileName = fileName;
            Number = number;
            Description = description;
            ClientID = clientID;
        }

        public string ClientName { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public string FileName { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public string ClientID { get; set; }
    }

    public class InvoicesApplicationViewModel : ApplicationViewModel
    {        
        public InvoicesApplicationViewModel(MainViewModel mainViewModel) : base(mainViewModel)
        {                       
        }

        public ICommand ShowNewInvoiceFormCommand
        {
            get
            {
                return null;
            }
        }


        private IList<Invoice> _Invoices;
        public IList<Invoice> Invoices
        {
            get
            {
                if (_Invoices == null)
                    _Invoices = CreateInvoiceList();
                return _Invoices;
            }
        }

        private Invoice _SelectedInvoice;
        public Invoice SelectedInvoice
        {
            get { return _SelectedInvoice; }
            set
            {
                SetPropertyValue(value, ref _SelectedInvoice, "SelectedInvoice");
            }
        }

        private IList<Invoice> CreateInvoiceList()
        {
            var list = new List<Invoice>
            {
                new Invoice(DateTime.Today, "Will E. Otty", 1250, "Invoice1", 21, "Web Development", "001232412"),
                new Invoice(DateTime.Today.AddDays(-1), "Micro Service Advantage", 4500, "Invoice2", 22, "Web Design", "439128129"),
                new Invoice(DateTime.Today.AddDays(-10), "Acme Construction", 7500, "Invoice3", 23, "Web Design and Site Development", "2102811298"),
                new Invoice(DateTime.Today.AddDays(-12), "Custom Development Services", 500, "Invoice4", 24, "Computer Cleanup", "482082937"),
                new Invoice(DateTime.Today.AddDays(-14), "CERT", 1500, "Invoice5", 25, "Computer Virus Cleanup", "1039232839"),
                new Invoice(DateTime.Today.AddDays(-15), "R Entertainment Inc.", 1000, "Invoice6", 26, "Web Site Update", "9402382323"),
                new Invoice(DateTime.Today.AddDays(-15), "Shift Enterprises", 1700, "Invoice7", 27, "Web Site Update", "210323998"),
                new Invoice(DateTime.Today.AddDays(-15), "Will E. Otty", 2250, "Invoice8", 28, "Web Development", "092392378"),
                new Invoice(DateTime.Today.AddDays(-16), "Micro Service Advantage", 2580, "Invoice9", 29, "Web Design", "79789203289"),
                new Invoice(DateTime.Today.AddDays(-16), "Acme Construction", 7100, "Invoice10", 30, "Web Site Update", "542093891"),
                new Invoice(DateTime.Today.AddDays(-18), "Custom Development Services", 1500, "Invoice11", 31, "Computer Cleanup", "0991023089"),
                new Invoice(DateTime.Today.AddDays(-19), "CERT", 1900, "Invoice12", 32, "Web Development", "5008393279"),
                new Invoice(DateTime.Today.AddDays(-20), "R Entertainment Inc.", 1010, "Invoice13", 33, "Web Site Update", "40189232983"),
                new Invoice(DateTime.Today.AddDays(-21), "Shift Enterprises", 1900, "Invoice14", 34, "Web Design and Site Development", "9038923987"),
            };

            SelectedInvoice = list[3];

            return list;
        }
    }
}
