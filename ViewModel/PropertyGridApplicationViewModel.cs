﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace MetroOSSample
{
	class PropertyGridApplicationViewModel : ApplicationViewModel
	{
        public PropertyGridApplicationViewModel(MainViewModel mainViewModel)
            : base(mainViewModel)
        {
            
        }

        private object _SelectedObject;
        public object SelectedObject
        {
            get { return _SelectedObject; }
            set { SetPropertyValue(value, ref _SelectedObject, "SelectedObject"); }
        }
	}
}
