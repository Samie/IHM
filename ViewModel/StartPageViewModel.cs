﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace MetroOSSample
{
    public class StartPageViewModel
    {
        public StartPageViewModel()
        {
            Applications = new ObservableCollection<ApplicationViewModel>();
        }

        public ObservableCollection<ApplicationViewModel> Applications { get; private set; }
    }
}
