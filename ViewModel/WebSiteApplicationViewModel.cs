﻿
using System;
namespace MetroOSSample
{
    public class WebSiteApplicationViewModel : ApplicationViewModel
    {
        public WebSiteApplicationViewModel(MainViewModel mainViewModel)
            : base(mainViewModel)
        {

        }

        public Uri Url { get; set; }

    }
}
