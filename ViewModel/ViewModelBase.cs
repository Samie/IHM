﻿using System;
using System.ComponentModel;

namespace MetroOSSample
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and clean up of resources such as event instanceHandlers. This class is abstract.
    /// </summary>
    public abstract class ViewModelBase : IDisposable, INotifyPropertyChanged
    {
        #region Constructor / Fields 

        private string _DisplayName;

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected ViewModelBase()
        {
        }
        /// <summary>
        /// Construct with display name.
        /// </summary>
        protected ViewModelBase(string displayName) 
        {
            _DisplayName = displayName;        
        }

        #endregion // Constructor
        
        #region Public Properties

        /// <summary>
        /// Get or set the display name.
        /// </summary>
        public string DisplayName
        {
            get { return _DisplayName ?? (_DisplayName = this.GetType().Name); }
            set 
            {
                SetPropertyValue(value, ref _DisplayName, "DisplayName"); 
            }
        } 

        #endregion // Constructor

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
       internal protected virtual void OnPropertyChanged(string propertyName)
        {
            //VerifyPropertyName(propertyName);
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        /// <summary>
        /// Generic method to set a property value with equality 
        /// checking and raising the property changed event.
        /// </summary>
        internal protected bool SetPropertyValue<T>(T value, ref T field, string propertyName)
        {
            if ((value != null && !value.Equals(field)) || (value == null && field != null))
            {
                field = value;
                if(propertyName != null)
                    OnPropertyChanged(propertyName);
                return true;
            }
            return false;
        }

        
        #endregion // INotifyPropertyChanged Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Child classes can override this method to perform 
        /// clean-up logic, such as removing event instanceHandlers.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
        }

        #if DEBUG
        /// <summary>
        /// Useful for ensuring that ViewModel objects are properly garbage collected.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        ~ViewModelBase()
        {
            string msg = string.Format("{0} ({1}) ({2}) Finalized", this.GetType().Name, this.DisplayName, this.GetHashCode());
            System.Diagnostics.Debug.WriteLine(msg);
        }
        #endif
    }
}
