﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;

namespace MetroOSSample
{
    public class NotImplementedApplicationViewModel : ApplicationViewModel
    {
        public NotImplementedApplicationViewModel(MainViewModel mainViewModel) : base(mainViewModel)
        {
        }
        public Brush Background { get; set; }

        public ICommand OkCommand
        {
            get { return MainViewModel.ReturnToStartCommand; }
        }
    }
}
