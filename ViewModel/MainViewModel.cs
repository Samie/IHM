﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DevComponents.WPF.Controls;
using DevComponents.WPF.Metro;
using System.Windows;

namespace MetroOSSample
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            StartPages = CreateStartPages();
            Applications = CreateApplicationsList();
            Users = CreateUsersList();
            Login = new LoginViewModel(this);
            LoginSliderState = SlidingContentControlState.Normal;

            _LoggedInUser = new UserViewModel(this);
        }

        private MetroTheme _MetroTheme;
        public MetroTheme MetroTheme
        {
            get { return _MetroTheme; }
            set { SetPropertyValue(value, ref _MetroTheme, "MetroTheme"); }
        }

        private UserViewModel _LoggedInUser;
        public UserViewModel LoggedInUser
        {
            get { return _LoggedInUser; }
            set
            {
                if (SetPropertyValue(value, ref _LoggedInUser, "LoggedInUser"))
                {
                    if (value != null)
                        LoginSliderState = SlidingContentControlState.DockedLeft;
                    else
                        LoginSliderState = SlidingContentControlState.Normal;
                }
            }
        }

        private ApplicationViewModel _CurrentApplication;
        public ApplicationViewModel CurrentApplication
        {
            get { return _CurrentApplication; }
            set { SetPropertyValue(value, ref _CurrentApplication, "CurrentApplication"); }
        }

        public ObservableCollection<ApplicationViewModel> Applications { get; private set; }
        public ObservableCollection<StartPageViewModel> StartPages { get; private set; }
        public ObservableCollection<UserViewModel> Users { get; private set; }

        public LoginViewModel Login { get; private set; }

        private SlidingContentControlState _LoginSliderState;
        public SlidingContentControlState LoginSliderState
        {
            get { return _LoginSliderState; }
            set { SetPropertyValue(value, ref _LoginSliderState, "LoginSliderState"); }
        }

        private ICommand _ReturnToStartCommand;
        public ICommand ReturnToStartCommand
        {
            get
            {
                if (_ReturnToStartCommand == null)
                    _ReturnToStartCommand = new RelayCommand(
                        p => ReturnToStart(),
                        p => CurrentApplication != null);
                return _ReturnToStartCommand;
            }
        }

        public void ReturnToStart()
        {
            if (CurrentApplication == null) 
                return;
            CurrentApplication.IsActive = false;
            CurrentApplication = null;
        }

        private ObservableCollection<UserViewModel> CreateUsersList()
        {
            var apps = new ObservableCollection<UserViewModel>();

            apps.Add(new UserViewModel(this)
                {
                    FirstName = "Admin",
                    UserName = "Admin",
                    ImageSource = new BitmapImage(new Uri(@"/Images/user.png", UriKind.RelativeOrAbsolute))
                });

            apps.Add(new UserViewModel(this)
            {
                FirstName = "Client",
                UserName = "Client",
                ImageSource = new BitmapImage(new Uri(@"/Images/user.png", UriKind.RelativeOrAbsolute))
            });

            return apps;
        }

        private ObservableCollection<StartPageViewModel> CreateStartPages()
        {
            var pages = new ObservableCollection<StartPageViewModel>();

            var page1 = new StartPageViewModel();
            var invoicesApp = new InvoicesApplicationViewModel(this)
                {
                    MetroTheme = DevComponents.WPF.Metro.MetroTheme.Cherry,
                    Title = "Invoices",
                    Description = "  ",
                    TileColor = MetroTileColor.Coffee,
                    ImageSource = new BitmapImage(new Uri(@"/Images/Invoice.png", UriKind.RelativeOrAbsolute))
                };
            invoicesApp.TileFrames.Add(new TileFrameViewModel 
            { 
                Description = "University Managmment",
                TileColor = MetroTileColor.None,
                DisplayDuration = new Duration(TimeSpan.FromSeconds(5)),
                ImageSource = new BitmapImage(new Uri(@"/Images/TransparentTileImage.png", UriKind.RelativeOrAbsolute)) 
            });
            invoicesApp.TileFrames.Add(new TileFrameViewModel
            {
                Description = "Student Managmment",
                TileColor = MetroTileColor.None,
                DisplayDuration = new Duration(TimeSpan.FromSeconds(5)),
                ImageSource = new BitmapImage(new Uri(@"/Images/TransparentTileImage.png", UriKind.RelativeOrAbsolute))
            });
            invoicesApp.TileFrames.Add(new TileFrameViewModel
            {
                Description = "But it has some of it.",
                TileColor = MetroTileColor.None,
                DisplayDuration = new Duration(TimeSpan.FromSeconds(5)),
                ImageSource = new BitmapImage(new Uri(@"/Images/TransparentTileImage.png", UriKind.RelativeOrAbsolute))
            });

            page1.Applications.Add(invoicesApp);

            page1.Applications.Add(new WebSiteApplicationViewModel(this)
                {
                    Title = "Web",
                    //Description = "Visit the DevComponents Web Site",
                    TileType = MetroTileType.Small,
                    ImageSource = new BitmapImage(new Uri(@"/Images/Web.png", UriKind.RelativeOrAbsolute)),
                    TileColor = MetroTileColor.Orange,
                    Url = new Uri(@"http://www.univ-msila.com", UriKind.Absolute),
                    MetroTheme = MetroTheme.BlackSky
                });

            page1.Applications.Add(new WebSiteApplicationViewModel(this)
            {
                Url = new Uri(@"http://www.univ-msila.com", UriKind.Absolute),
                Title = "KB",
                TileType = MetroTileType.Small,
                //Description = "Go to the help article for Metro UI.",
                ImageSource = new BitmapImage(new Uri(@"/Images/Help.png", UriKind.RelativeOrAbsolute)),
                TileColor = MetroTileColor.Green,
                MetroTheme = MetroTheme.SilverGreen
            });

            page1.Applications.Add(new FileSystemApplicationViewModel(this)
            {
                Title = "File Browser",
                Description = "Browse files on your computer",
                ImageSource = new BitmapImage(new Uri(@"/Images/Document.png", UriKind.RelativeOrAbsolute)),
                TileColor = MetroTileColor.RedOrange,
                MetroTheme = DevComponents.WPF.Metro.MetroTheme.Brown,
            });

            page1.Applications.Add(new PropertyGridApplicationViewModel(this)
            {
                Title = "Properties Editor",
                Description = "View Property Grid",
                ImageSource = new BitmapImage(new Uri(@"/Images/Invoice.png", UriKind.RelativeOrAbsolute)),
                TileColor = MetroTileColor.Blueish,
                MetroTheme = DevComponents.WPF.Metro.MetroTheme.Bordeaux,
            });

            page1.Applications.Add(new NotImplementedApplicationViewModel(this)
                {
                    Title = "Control Panel",
                    Description = "Edit application settings",
                    ImageSource = new BitmapImage(new Uri(@"/Images/Settings.png", UriKind.RelativeOrAbsolute)),
                    Background = Brushes.BlanchedAlmond,
                    TileColor = MetroTileColor.Maroon,
                    MetroTheme = MetroTheme.DarkRed
                });

            var themeExplorerApp = new MetroThemeExplorerApplicationViewModel(this)
            {
                Title = "IHM Theme Explorer",
                Description = "Explore the color themes.",
                ImageSource = new BitmapImage(new Uri(@"/Images/Paint.png", UriKind.RelativeOrAbsolute)),
                TileColor = MetroTileColor.Magenta,
                MetroTheme = MetroTheme.BlueishBrown
            };
            themeExplorerApp.TileFrames.Add(
                new TileFrameViewModel
                {

                    Title = "Management Bibliotheque",
                    Description = "Explore the books of themes.",
                    ImageSource = new BitmapImage(new Uri(@"/Images/Paint.png", UriKind.RelativeOrAbsolute)),
                    TileColor = MetroTileColor.Magenta,
                    DisplayDuration = new Duration(TimeSpan.FromSeconds(6))
                });
            themeExplorerApp.TileFrames.Add(
                new TileFrameViewModel
                {
                    Description = "THERE",
                    TileColor = MetroTileColor.Green,
                    DisplayDuration = new Duration(TimeSpan.FromSeconds(1.4))
                });
            themeExplorerApp.TileFrames.Add(
                new TileFrameViewModel
                {
                    Description = "ARE LOTS",
                    TileColor = MetroTileColor.DarkOlive,
                    DisplayDuration = new Duration(TimeSpan.FromSeconds(1.4))
                });
            themeExplorerApp.TileFrames.Add(
                new TileFrameViewModel
                {
                    Description = "OF CHOICES",
                    TileColor = MetroTileColor.DarkBlue,
                    DisplayDuration = new Duration(TimeSpan.FromSeconds(1.4))
                });
            page1.Applications.Add(themeExplorerApp);

            pages.Add(page1);

            var page2 = new StartPageViewModel();

            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "IHM TP",
                TileType = MetroTileType.Small,
                Description = "Charik",
                TileColor = MetroTileColor.Coffee,
                MetroTheme = MetroTheme.BlackLilac
            });
            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "ihm Application",
                TileType = MetroTileType.Small,
                Description = "msila univ",
                TileColor = MetroTileColor.Yellowish,
                MetroTheme = MetroTheme.BlackLilac
            });
            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Master SIGL",
                TileType = MetroTileType.Small,
                Description = "Master SIGL",
                TileColor = MetroTileColor.Yellow,
                MetroTheme = MetroTheme.BlackLilac
            });
            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Courses",
                TileType = MetroTileType.Small,
                Description = "Courses",
                TileColor = MetroTileColor.Teal,
                MetroTheme = MetroTheme.BlackLilac
            });
            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Learning",
                TileType = MetroTileType.Small,
                Description = "Learning IHM project",
                TileColor = MetroTileColor.RedViolet,
                MetroTheme = MetroTheme.BlackLilac
            });
            page2.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Courses",
                TileType = MetroTileType.Small,
                Description = "Demo",
                TileColor = MetroTileColor.Rust,
                MetroTheme = MetroTheme.BlackLilac
            });

            pages.Add(page2);

            var page3 = new StartPageViewModel();

            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.RedOrange,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.PlumWashed,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.Plum,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                TileType = MetroTileType.Small,
                Description = "Demo",
                TileColor = MetroTileColor.Orange,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                TileType = MetroTileType.Small,
                Description = "Demo",
                TileColor = MetroTileColor.Olive,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                TileType = MetroTileType.Small,
                Description = "Demo",
                TileColor = MetroTileColor.MaroonWashed,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                TileType = MetroTileType.Small,
                Description = "Demo",
                TileColor = MetroTileColor.Maroon,
                MetroTheme = MetroTheme.BlackLilac
            });
            page3.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.Orange,
                MetroTheme = MetroTheme.BlackLilac
            });

            pages.Add(page3);

            var page4 = new StartPageViewModel();

            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.RedOrange,
                MetroTheme = MetroTheme.BlackLilac
            });
            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.PlumWashed,
                MetroTheme = MetroTheme.BlackLilac
            });
            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.Plum,
                MetroTheme = MetroTheme.BlackLilac
            });
            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.Orange,
                MetroTheme = MetroTheme.BlackLilac
            });
            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Sample App",
                Description = "Unimplemented Application for Demonstration",
                TileColor = MetroTileColor.Olive,
                MetroTheme = MetroTheme.BlackLilac
            });
            page4.Applications.Add(new NotImplementedApplicationViewModel(this)
            {
                Title = "Contact",
                Description = "Contact us",
                TileColor = MetroTileColor.MaroonWashed,
                MetroTheme = MetroTheme.BlackLilac
            });

            pages.Add(page4);

            return pages;
        }

        private ObservableCollection<ApplicationViewModel> CreateApplicationsList()
        {
            var apps = new ObservableCollection<ApplicationViewModel>();

            foreach (var page in StartPages)
            {
                foreach (var app in page.Applications)
                {
                    apps.Add(app);
                }
            }

            return apps;
        }
    }
}
