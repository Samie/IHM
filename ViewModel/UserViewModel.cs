﻿using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Input;
using DevComponents.WPF.Controls;
using DevComponents.WPF.Metro;

namespace MetroOSSample
{
    public class UserViewModel : MetroIdentity
    {
        private readonly MainViewModel _MainViewModel;
        public UserViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
        }

        public SecureString Password
        {
            get
            {
                // Create secure string on demand. 
                // Typically the text will be obtained from storage
                // in encrypted form, decrypted and placed into the secure string.
                var decryptedText = "pass";

                var securePassword = new SecureString();
                foreach(var c in decryptedText)
                    securePassword.AppendChar(c);
                return securePassword;
            }
            set
            {
                // Above in reverse. Get text from secure string, encrypt it and place into storage.
            }
        }


        //private SecureString _SecurePassword;
        //public SecureString SecurePassword
        //{
        //    get { return _SecurePassword; }
        //    set
        //    {
        //        if (value == _SecurePassword)
        //            return;
        //        LoginErrorMessage = null;
        //        _SecurePassword = value;
        //        OnPropertyChanged("SecurePassword");
        //    }
        //}

        //private bool _IsValidPassword;

        //public SecureString SecurePassword
        //{
        //    get { return new SecureString(); }
        //    set
        //    {
        //        _IsValidPassword = ValidatePassword(value);
        //        OnPropertyChanged("SecurePassword");
        //    }
        //}
        
        //private ICommand _LoginCommand;
        //public ICommand LoginCommand
        //{
        //    get { return _LoginCommand ?? (_LoginCommand = new RelayCommand(AttemptLogin)); }
        //}

        private ICommand _LogoutCommand;
        public ICommand LogoutCommand
        {
            get { return _LogoutCommand ?? (_LogoutCommand = new RelayCommand(p => _MainViewModel.LoggedInUser = null)); }
        }

        //private string _LoginErrorMessage;
        //public string LoginErrorMessage
        //{
        //    get { return _LoginErrorMessage; }
        //    set
        //    {
        //        if (value == _LoginErrorMessage)
        //            return;
        //        _LoginErrorMessage = value;
        //        OnPropertyChanged("LoginErrorMessage");
        //    }       
        //}

        //private void AttemptLogin(object p)
        //{
        //    if (_IsValidPassword)
        //    {
        //        _LoginErrorMessage = null;
        //        _MainViewModel.LoggedInUser = this;
        //        SecurePassword = null;
        //    }
        //    else
        //    {
        //        LoginErrorMessage = "Invalid password. (password is 'pass')";
        //    }
        //}

        //private static bool ValidatePassword(SecureString securePassword)
        //{
        //    return SecureStringToManagedString(securePassword) == "pass";
        //}
        
        //[SecuritySafeCritical]
        //private static string SecureStringToManagedString(SecureString secure)
        //{
        //    if (secure == null)
        //        return null;
        //    var p = Marshal.SecureStringToBSTR(secure);
        //    var value = Marshal.PtrToStringBSTR(p);
        //    Marshal.ZeroFreeBSTR(p);
        //    return value;
        //}
    }
}
