﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Input;
using System.Windows.Threading;
using DevComponents.WPF.Controls;

namespace MetroOSSample
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly MainViewModel _MainViewModel;
        public LoginViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
        }

        public object AllUsers { get { return _MainViewModel.Users; } }

        private UserViewModel _CurrentUser;
        public UserViewModel CurrentUser
        {
            get { return _CurrentUser; }
            set { SetPropertyValue(value, ref _CurrentUser, "CurrentUser"); }
        }

        private ICommand _NavigateBackCommand;
        public ICommand NavigateBackCommand
        {
            get
            {
                if (_NavigateBackCommand == null)
                    _NavigateBackCommand = new RelayCommand(NavigateBack);
                return _NavigateBackCommand;
            }
        }

        private ICommand _LoginCommand;
        public ICommand LoginCommand
        {
            get { return _LoginCommand ?? (_LoginCommand = new RelayCommand(AttemptLogin, p => !string.IsNullOrEmpty(PasswordBoxText))); }
        }

        private string _LoginErrorMessage;
        public string LoginErrorMessage
        {
            get { return _LoginErrorMessage; }
            set
            {
                _LoginErrorMessage = value;
                OnPropertyChanged("LoginErrorMessage");
            }
        }

        private SecureString _UserEnteredPassword;
        public SecureString UserEnteredPassword
        {
            get { return _UserEnteredPassword; }
            set
            {
                if (value == _UserEnteredPassword)
                    return;
                _UserEnteredPassword = value;
                OnPropertyChanged("UserEnteredPassword");
            }
        }

        private string _PasswordBoxText;
        // Text of the password box changes when user types. Use this to clear error message
        // after user begins to try to correct error. Note the text only consists of the password char character, not the password itself.
        public string PasswordBoxText
        {
            get { return _PasswordBoxText; }
            set
            {
                _PasswordBoxText = value;
                LoginErrorMessage = null;
            }
        }

        private void AttemptLogin(object p)
        {
            if (CurrentUser == null)
            {
                NavigateBack(null);
                return;
            }

            if (AreEqual(UserEnteredPassword, CurrentUser.Password))
            {
                _LoginErrorMessage = null;
                _MainViewModel.LoggedInUser = CurrentUser;
                CurrentUser = null;
                UserEnteredPassword = null;
            }
            else
            {
                LoginErrorMessage = "Invalid password. (password is 'pass')";
            }
        }

        private void NavigateBack(object p)
        {
            CurrentUser = null;
            UserEnteredPassword = null;
            LoginErrorMessage = null;
        }

        private static bool AreEqual(SecureString s1, SecureString s2)
        {
            if (s1 == null)
                return s2 == null;
            if (s2 == null)
                return false;
            if (s1.Length != s2.Length)
                return false;

            return SecureStringToManagedString(s1) == SecureStringToManagedString(s2);
        }

        [SecuritySafeCritical]
        private static string SecureStringToManagedString(SecureString secure)
        {
            if (secure == null)
                return null;
            var p = Marshal.SecureStringToBSTR(secure);
            var value = Marshal.PtrToStringBSTR(p);
            Marshal.ZeroFreeBSTR(p);
            return value;
        }
    }
}
