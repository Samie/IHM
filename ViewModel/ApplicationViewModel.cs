﻿using System.Windows.Input;
using System.Windows.Media;
using DevComponents.WPF.Controls;
using DevComponents.WPF.Metro;
using System.Windows;
using System.Collections.ObjectModel;
using System;

namespace MetroOSSample
{
    public class TileFrameViewModel : ViewModelBase
    {
        public TileFrameViewModel()
        {
            DisplayDuration = new Duration(TimeSpan.FromSeconds(3));
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { SetPropertyValue(value, ref _Title, "Title"); }
        }

        private object _Description;
        public object Description
        {
            get { return _Description; }
            set { SetPropertyValue(value, ref _Description, "Description"); }
        }

        public ImageSource ImageSource { get; set; }

        public MetroTileColor TileColor { get; set; }

        public Duration DisplayDuration { get; set; }
    }

    public class ApplicationViewModel : ViewModelBase
    {
        public MainViewModel MainViewModel { get; private set; }
        public ApplicationViewModel(MainViewModel mainViewModel)
        {
            MainViewModel = mainViewModel;
        }

        private object _Description;
        public object Description
        {
            get { return _Description; }
            set { SetPropertyValue(value, ref _Description, "Description"); }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { SetPropertyValue(value, ref _Title, "Title"); }
        }

        public MetroTileType TileType { get; set; }

        private MetroTheme? _MetroTheme;
        public MetroTheme? MetroTheme
        {
            get { return _MetroTheme; }
            set
            {
                SetPropertyValue(value, ref _MetroTheme, "MetroTheme");
            }
        }

        public ImageSource ImageSource { get; set; }

        public MetroTileColor TileColor { get; set; }

        private bool _IsActive;
        public bool IsActive
        {
            get { return _IsActive; }
            set
            {
                if (SetPropertyValue(value, ref _IsActive, "IsActive"))
                {
                    if(value)
                        MainViewModel.CurrentApplication = this;
                }
            }
        }

        private ICommand _ActivateCommand;
        public ICommand ActivateCommand
        {
            get
            {
                if (_ActivateCommand == null)
                {
                    _ActivateCommand = new RelayCommand(p => IsActive = true, p => IsActive == false);
                }
                return _ActivateCommand;
            }
        }

        public ICommand ReturnToStartCommand
        {
            get { return MainViewModel.ReturnToStartCommand; }
        }

        private ObservableCollection<TileFrameViewModel> _TileFrames;
        public ObservableCollection<TileFrameViewModel> TileFrames
        {
            get
            {
                if (_TileFrames == null)
                    _TileFrames = new ObservableCollection<TileFrameViewModel>();
                return _TileFrames;
            }
        }

    }
}
