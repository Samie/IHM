﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using DevComponents.WPF.Controls;
using System.Windows.Threading;

namespace MetroOSSample
{
    public class WebBrowserBehavior
    {

        /// <summary>
        /// Using a DependencyProperty as the backing store for Url.
        /// </summary>
        public static readonly DependencyProperty UrlProperty = DependencyProperty.RegisterAttached("Url", typeof(Uri), typeof(WebBrowserBehavior), new UIPropertyMetadata(null, OnUrlChanged));
        private static void OnUrlChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var browser = (WebBrowser)s;
            var url = e.NewValue as Uri;
            if (url != null)
                browser.Source = url;
            browser.LoadCompleted += delegate
            {
                browser.Width = Double.NaN;
            };
        }
        public static Uri GetUrl(WebBrowser obj)
        {
            return (Uri)obj.GetValue(UrlProperty);
        }
        public static void SetUrl(WebBrowser obj, Uri value)
        {
            obj.SetValue(UrlProperty, value);
        }
        
    }
}
