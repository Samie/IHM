﻿using System.Windows.Input;

namespace MetroOSSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);
            if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Shift)
            {
                var vm = (MainViewModel)MainGrid.DataContext;
                vm.ReturnToStart();
                e.Handled = true;
            }
        }

    }
}
